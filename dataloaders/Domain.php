<?php

namespace app\dataloaders;

use app\components\Config;
use app\dataloaders\Api;

/**
 * Websupport domain dataloader.   
 */
class Domain {
  /**
   * @var string api path
   */     
  const API_PATH = '/user/self/zone';
  /**
   * @var object saved instance
   */
  protected static $_instance;
  /**
   * @var object component config
   */     
  private $config;
  /**
   * @var object dataloader api
   */     
  private $api;
  /**
   * @var string domain name
   */      
  private $name;
  
    /**
     * Init class.
     */         
    public function __construct() {
      $this->config = Config::gi();
      $this->api = Api::gi();
      
      $this->name = $this->config->read( 'domain_name' );
    }

    /**
     * Get class instance.
     * 
     * @return object saved instance
     */
    public static function gi() {	
      self::$_instance = self::$_instance === null ? new self() : self::$_instance;

      return self::$_instance;
    }
    
    /**
     * Get all records.
     *
     * @param number page      
     * @return array all records
     */                   
    public function getAllRecords( $page = 0 ) {
      $records = $this->call( 'GET', '/record', array( 'page' => $page ) );

      return !empty( $records->items ) ? $records->items : array();
    }
    
    /**
     * Add record.
     * 
     * @param array record params
     * @return boolean is added
     */                             
    public function addRecord( $params = array() ) {
      $this->call( 'POST', '/record', $params );

      return $this->api->getCode() == 201 ? true : false;
    }
    
    /**
     * Delete record.
     * 
     * @param number record id
     * @return boolean is deleted
     */                        
    public function deleteRecord( $id = 0 ) {
      $this->call( 'DELETE', '/record/'.$id );

      return $this->api->getCode() == 200 ? true : false;
    }
    
    /**
     * Call request.
     * 
     * @param string request type
     * @param string request action
     * @param array request params
     * @return array request result
     */                                  
    private function call( $type = '', $action = '', $params = array() ) {
      return $this->api->call( $type, self::API_PATH.'/'.$this->name.$action, $params );
    }
}

?>