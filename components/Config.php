<?php

namespace app\components;

/**
 * This component work with configs.  
 */ 
class Config {
  /**
   * @var string path to configs
   */
  const PATH = ROOT.'/configs';
  /**
   * @var string sub-path to development configs
   */
  const DEV_PATH = '/development';	
  /**
   * @var object saved instance
   */
  protected static $_instance;

    /**
     * Get class instance.
     * 
     * @return object saved instance
     */
    public static function gi() {
      self::$_instance = self::$_instance === null ? new self() : self::$_instance;

      return self::$_instance;
    }

    /**
     * Read parameter in main config file.
     * 
     * @param string config parameter
     * @return mixed config value
     */
    public function read( $param = '' ) {
      $config = $this->get( 'main' );

      return array_key_exists( $param, $config ) ? $config[$param] : '';
    }

    /**
     * Get config file. If user is developer and exist same config
     * in development folder so merge their.
     * 
     * @param string config name
     * @return array config data
     */
    public function get( $name = '' ) {
      $result = array();

      @include self::PATH.'/'.$name.'.php';

      $result = !empty( $config ) ? $config : $result;

        if ( $this->isDeveloper() ):
          @include self::PATH.self::DEV_PATH.'/'.$name.'.php';

          $result = !empty( $config ) ? array_merge( $result, $config ) : $result;
        endif;

      return $result;
    }

    /**
     * Is localhost server than user is developer.
     * 
     * @return boolean is developer
     */
    public function isDeveloper() {
      return ( !empty( $_SERVER['REMOTE_ADDR'] ) AND $_SERVER['REMOTE_ADDR'] == '127.0.0.1' ) ? true : false;
    }
}

?>