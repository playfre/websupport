<?php

namespace app\components;

use app\components\Config;
use app\components\General;
use app\dataloaders\Api;
use app\dataloaders\Domain;

/**
 * This component work with app.   
 */
class App {
  /**
   * @var string path to views
   */
  const VIEWS_PATH = ROOT.'/views';	
  /**
   * @var object component config
   */
  protected $config;
  /**
   * @var object component general
   */     
  protected $general;
  /**
   * @var object dataloader api
   */     
  protected $api;
  /**
   * @var object dataloder domain
   */     
  protected $domain;
  /**
   * @var string request uri
   */
  private $uri = '';
  /**
   * @var string view
   */
  private $view = '';
  /**
   * @var array view parameters
   */
  private $viewParams = array();
  /**
   * @var array ajax request json data
   */
  private $json = array();

    /**
     * Init class.
     */
    public function __construct() {
      $this->config = Config::gi();
      $this->general = General::gi();
      $this->api = Api::gi();
      $this->domain = Domain::gi();

      $this->uri = !empty( $_SERVER['REQUEST_URI'] ) ? $_SERVER['REQUEST_URI'] : '';
    }
    
    /**
     * Finish action, render app.
     */         
    public function __destruct() {
      $this->render();
    }

    /**
     * Set view.
     *
     * @param string view name
     */
    public function setView( $name = '' ) {
      $this->view = $name;
    }

    /**
     * Set view parameter.
     *
     * @param string parameter name
     * @param mixed parameter value
     */
    public function set( $param = '', $value = '' ) {
      $this->viewParams[$param] = $value;
    }

    /**
     * Push value into ajax json. This json rendered in ajax requests.
     * 
     * @param string parameter name
     * @param mixed parameter value
     */
    public function push( $param = '', $value = '' ) {
      $this->json[$param] = $value;
    }

    /**
     * Set ajax request status to success.
     */
    public function success() {
      $this->push( 'status', 'success' );
    }

    /**
     * Render view and his parameters. If this method call in main controller so render layout with assets.
     * Else this method call in ajax controller so render ajax json.     
     */
    public function render() {
      $view = $this->renderView( $this->view, $this->viewParams );

        if ( !strpos( $this->uri, 'ajax' ) ):
          /**
           * Main controller.
           */                     
          $config = $this->config->get( 'assets' );

          $assets = $this->renderElement( 'css', array( 'assets' => $config['css'] ) );
          $assets .= $this->renderElement( 'js', array( 'assets' => $config['js'] ) );

          echo $this->renderLayout( array( 'assets' => $assets, 'view' => $view ) );
        else:
          /**
           * Ajax controller.
           */                     
          $this->push( 'render', $view );

          $this->setView( 'json' );

          echo $this->renderView( $this->view );
        endif;
    }

    /**
     * Render element.
     *
     * @param string element name
     * @param array element parameters
     * @return string rendered element
     */
    public function renderElement( $name = '', $params = array() ) {
      return $this->baseRender( '/elements/'.$name, $params );
    }

    /**
     * Render view.
     *
     * @param string view name
     * @param array view parameters
     * @return string rendered view
     */
    public function renderView( $name = '', $params = array() ) {
      return $this->baseRender( '/main/'.$name, $params );
    }

    /**
     * Render layout.
     *
     * @param array layout parameters
     * @return string rendered layout
     */
    public function renderLayout( $params = array() ) {
      return $this->baseRender( '/layout', $params );
    }

    /**
     * Base render file.
     * 
     * @param string file route
     * @param array file parameters
     * @return string rendered file.
     */
    public function baseRender( $route = '', $params = array() ) {
      $result = '';

      $route = self::VIEWS_PATH.$route.'.phtml';

        if ( file_exists( $route ) ):
          ob_start();

          extract( $params, EXTR_PREFIX_SAME, 'wddx' );

          include $route;

          $result = ob_get_clean();
        endif;

      return $result;
    }
    
    /**
     * Not found page, redirect to home.
     */         
    public function notFound() {
      $this->general->goHome();
    }
}

?>