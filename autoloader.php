<?php

/**
 * Autoloader use to include.
 */ 
spl_autoload_register(
  function( $class ) {
    $class = str_replace( '\\', '/', $class );
    $class = str_replace( 'app', ROOT, $class );

    @include $class.'.php';
  }
);

?>