<?php

namespace app\components;

use app\components\Config;

/**
 * This component has general methods.
 */
class General {
  /**
   * @var object saved instance
   */
  protected static $_instance;
  /**
   * @var object component config
   */
  private $config;

    /**
     * Init class.
     */
    public function __construct() {
      $this->config = Config::gi();
    }

    /**
     * Get class instance.
     * 
     * @return object saved instance
     */
    public static function gi() {
      self::$_instance = self::$_instance === null ? new self() : self::$_instance;

      return self::$_instance;
    }

    /**
     * Redirect to home.
     */
    public function goHome() {
      $config = $this->config->get( 'main' );

      $this->redirect( $config['protocol'].'://'.$config['domain'] );
    }

    /**
     * Redirect to url.
     * 
     * @param string url
     */
    public function redirect( $url = '' ) {
      header( 'Location:'.$url, true, 301 );
    }
    
    /**
     * Build route for asset. If not set version in parameter
     * so set version from main config to suffix.
     * 
     * @param string asset route
     * @param string asset version     
     * @return string builded asset route
     */
    public function buildAssetRoute( $route = '', $version = '' ) {
      $version = !empty( $version ) ? $version : $this->config->read( 'assets_version' );
      
      return $this->config->read( 'assets_path' ).$route.'?v='.$version;
    }
}

?>