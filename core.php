<?php

namespace app;

use app\components\General;

define( 'ROOT', $_SERVER['DOCUMENT_ROOT'] );

require ROOT.'/autoloader.php';

$controllers = array( 'ajax' );

$uri = !empty( $_SERVER['REQUEST_URI'] ) ? $_SERVER['REQUEST_URI'] : '';
$uri = trim( preg_replace( array( '/[^a-zA-Z0-9\s-\/]/', '/  /' ), array( '', ' ' ), $uri ) );

$parse = explode( '/', $uri );
$parse = array_slice( $parse, 1 );

$controller = 'main';
$method = '';
$params = array();

  foreach ( $parse as $key => $value ):
      if ( empty( $key ) AND in_array( $value, $controllers ) ):
        $controller = strtoupper( $value[0] ).substr( $value, 1 );
      elseif ( empty( $method ) ):
        $explode = explode( '-', $value );

          foreach ( $explode as $part ):
            $method .= empty( $method ) ? $part : strtoupper( $part[0] ).substr( $part, 1 );
          endforeach;
      else:
        $params[] = $value;
      endif;
  endforeach;

$method = !empty( $method ) ? $method : 'index';
$alias = 'app\\controllers\\'.$controller;

  if ( class_exists( $alias ) ):
      if ( method_exists( $alias, $method ) ):
        $controller = new $alias();
        $controller->$method( $params );
      else:
        General::gi()->goHome();
      endif;
  else:
    General::gi()->goHome();
  endif;

?>