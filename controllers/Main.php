<?php

namespace app\controllers;

use app\components\App;

/**
 * This controller work with url requests.
 */
class Main extends App {

    /**
     * View of index.
     */
    public function index() {
      $records = $this->domain->getAllRecords( 1 );

      $this->setView( 'index' );
      
      $this->set( 'records', $records );
      $this->set( 'limit', $this->api->getLimit() );
    }
    
    /**
     * View of add domain record.
     */         
    public function addDomainRecord() {
      $type = !empty( $_POST['type'] ) ? $_POST['type'] : '';
      $name = !empty( $_POST['name'] ) ? $_POST['name'] : '';
      $content = !empty( $_POST['content'] ) ? $_POST['content'] : '';
      $ttl = ( !empty( $_POST['ttl'] ) AND $_POST['ttl'] > 0 ) ? $_POST['ttl'] : 600;
        
        if ( !empty( $type ) AND !empty( $name ) AND !empty( $content ) AND !empty( $ttl ) ):
            if ( $this->domain->addRecord( array( 'type' => $type, 'name' => $name, 'content' => $content, 'ttl' => $ttl ) ) ):
              $this->general->goHome();
            endif;
        endif;
      
      $this->setView( 'addDomainRecord' );
      
      $this->set( 'post', !empty( $_POST ) ? true : false );
      $this->set( 'type', $type );
      $this->set( 'name', $name );
      $this->set( 'content', $content );
      $this->set( 'ttl', $ttl );
    }
}

?>