$( document ).ready(function( $ ) {

  $( document ).on( 'click', '#index .content .record .button', function() {
    var $this = this;
    var id = $( this ).closest( '.record' ).attr( 'data-id' );
  
    callAjax( '/delete-domain-record', { id: id }, function( result ) {
        if ( result['status'] != undefined && result['status'] == 'success' ) {
          $( $this ).closest( '.record' ).hide();
        }
        else {
          alert( 'Odstránenie záznamu nebolo úspešné, skúste znova neskôr.' );
        }
    } );
  } );
  
  $( document ).on( 'click', '#index .buttons .button.load', function() {
    var $this = this;
    var page = $( '#index .page' ).val();
    
    callAjax( '/load-domain-records', { page: page }, function( result ) {
        if ( result['render'] != '' ) {
          $( '#index .content' ).append( result['render'] );
          
          $( '#index .page' ).val( parseInt( page ) + 1 );
        }
        else {
          $( $this ).hide();
        }
    } );
  } );

} );