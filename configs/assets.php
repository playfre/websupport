<?php

/**
 * Assets config. Keys is assets extension and their data
 * is files with this extension. Data key is asset name and his value is
 * asset version.  
 */
$config = array(
  /**
   * Css style assets.
   */
  'css' => array(
    'app' => '1.0.0',
  ),
  /**
   * Javascript assets.
   */
  'js' => array(
    'lib/jquery.min' => '1.0.0',
    'functions' => '1.0.0',
    'main' => '1.0.0',
  ),
);

?>