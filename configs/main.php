<?php

/**
 * Main production config.
 * This config is readable of config component.
 */
$config = array(
  /**
   * Project domain.
   */
  'domain' => 'apiquest.6f.sk',
  /**
   * Project protocol.
   */
  'protocol' => 'http',
  /**
   * Assets path.
   */     
  'assets_path' => '/assets',
  /**
   * Assets version.
   */
  'assets_version' => '1.0.0',
  /**
   * Username to websupport login.
   */     
  'api_username' => 'apiquest',
  /**
   * Password to websupport login.
   */     
  'api_password' => 'websupport',
  /**
   * Domain name with work it.
   */     
  'domain_name' => 'apiquest.sk',
);

?>