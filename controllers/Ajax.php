<?php

namespace app\controllers;

use app\components\App;

/**
 * This controller work with ajax requests.
 */
class Ajax extends App {

    /**
     * Delete domain record.
     * 
     * @param number record id
     */                   
    public function deleteDomainRecord() {
      $id = ( !empty( $_POST['id'] ) AND $_POST['id'] > 0 ) ? $_POST['id'] : 0;
    
        if ( $this->domain->deleteRecord( $id ) ):
          $this->success();
        endif;
    }
    
    /**
     * Load domain records.
     * 
     * @param number page
     */                   
    public function loadDomainRecords() {
      $page = ( !empty( $_POST['page'] ) AND $_POST['page'] > 0 ) ? $_POST['page'] : 0;
      
      $records = $this->domain->getAllRecords( $page );
      
      $html = $this->renderElement( 'record', array( 'records' => $records ) );
            
      $this->setView( 'html' );
      
      $this->set( 'content', $html );
    }
}

?>