<?php

namespace app\dataloaders;

use app\components\Config;

/**
 * Websupport api dataloader.   
 */
class Api {
  /**
   * @var string url
   */     
  const URL = 'https://rest.websupport.sk/v1';
  /**
   * @var number get data page limit
   */     
  const LIMIT = 10;
  /**
   * @var object saved instance
   */
  protected static $_instance;
  /**
   * @var object component config
   */     
  private $config;
  /**
   * @var string username to websupport login
   */     
  private $username = '';
  /**
   * @var string password to websupport login
   */     
  private $password = '';
  /**
   * @var number request http code
   */     
  private $code = 0;
  
    /**
     * Init class.
     */         
    public function __construct() {
      $this->config = Config::gi();
      
      $this->username = $this->config->read( 'api_username' );
      $this->password = $this->config->read( 'api_password' );
    }

    /**
     * Get class instance.
     * 
     * @return object saved instance
     */
    public static function gi() {	
      self::$_instance = self::$_instance === null ? new self() : self::$_instance;

      return self::$_instance;
    }
    
    /**
     * Call request.
     * 
     * @param string request type
     * @param string request action
     * @param array request params
     * @return array request result
     */                                  
    public function call( $type = '', $action = '', $params = array() ) {
      $result = array();

      $curl = curl_init();

      curl_setopt( $curl, CURLOPT_TIMEOUT, 60 );
      curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, $type );
      curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
      curl_setopt( $curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC );
      curl_setopt( $curl, CURLOPT_USERPWD, $this->username.':'.$this->password );
      curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
      
      $url = self::URL.$action;

        if ( !empty( $params ) ):
            if ( $type == 'GET' ):
              $params['pagesize'] = self::LIMIT;
              
              $uri = array();
              
                foreach ( $params as $name => $value ):
                  $uri[] = $name.'='.$value;
                endforeach;
              
              $url .= '?'.implode( '&', $uri );
            elseif ( $type == 'POST' ):
              curl_setopt( $curl, CURLOPT_POSTFIELDS, json_encode( $params ) );
            endif;
        endif;
 
      curl_setopt( $curl, CURLOPT_URL, $url );

      $result = json_decode( curl_exec( $curl ) );
      
      $this->code = curl_getinfo( $curl, CURLINFO_HTTP_CODE );

      curl_close( $curl );

      return $result;
    }
    
    /**
     * Get request http code.
     * 
     * @return number http code
     */                   
    public function getCode() {
      return $this->code;
    }
    
    /**
     * Get page limit.
     * 
     * @return number page limit
     */                   
    public function getLimit() {
      return self::LIMIT;
    }
}

?>