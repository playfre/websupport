/**
 * Call ajax request.
 * 
 * @param string url action
 * @param object post params
 * @param object callback function
 */     
function callAjax( action, params, callback ) {
  $.post( '/ajax' + action, params ).done( function( result ) {
    callback( JSON.parse( result ) );
  } );
};